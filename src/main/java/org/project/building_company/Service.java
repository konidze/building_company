package org.project.building_company;

import java.util.Scanner;

public class Service {
    public void getStart() {
        Scanner input = new Scanner(System.in);
        Variables variables = null;
        while (input.hasNext()) {
            variables = new Variables();
            variables.flourQuantity = input.nextInt();
            variables.flourCount = input.nextInt();
            variables.startApartmentAmount = input.nextInt();
            break;
        }
        System.out.println(getLogic(variables).toString());
    }

    public Variables getLogic(Variables variables) {
        variables.totalApartmentAmount = 0;
        int avgAmount = variables.startApartmentAmount;
        if (variables.flourQuantity % variables.flourCount == 0) {
            for (int i = 0; i < variables.flourQuantity / variables.flourCount; i++) {
                for (int j = 0; j < variables.flourCount; j++) {
                    variables.totalApartmentAmount = variables.totalApartmentAmount + avgAmount;
                }
                avgAmount = avgAmount + 1000;
            }
        } else {
            for (int i = 0; i < (variables.flourQuantity - variables.flourQuantity % variables.flourCount) / variables.flourCount; i++) {
                for (int j = 0; j < variables.flourCount; j++) {
                    variables.totalApartmentAmount = variables.totalApartmentAmount + avgAmount;
                }
                avgAmount = avgAmount + 1000;
            }
            variables.totalApartmentAmount = variables.totalApartmentAmount + (variables.flourQuantity % variables.flourCount) * avgAmount;
        }
        return variables;
    }
}
