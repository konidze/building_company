package org.project.building_company;

public class Variables {
    public int flourQuantity;
    public int flourCount;
    public int startApartmentAmount;
    public int totalApartmentAmount;

   public Variables(){
   }

    @Override
    public String toString() {
        return "Variables{" +
                "flourQuantity=" + flourQuantity +
                ", flourCount=" + flourCount +
                ", startApartmentAmount=" + startApartmentAmount +
                ", totalApartmentAmount=" + totalApartmentAmount +
                '}';
    }
}
